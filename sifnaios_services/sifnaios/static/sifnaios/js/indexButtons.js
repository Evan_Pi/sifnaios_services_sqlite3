//Scroll Down Button

$(document).ready(function() {
  $("#scrollDownAnimation").click(function() {
    $('html, body').animate({
      scrollTop: $(".wrapper").height()
    }, 800);
  });
});

$(document).ready(function() {
  $("#scrollDownAnimation2").click(function() {
    $('html, body').animate({
      scrollTop: $(".wrapper").height()
    }, 800);
  });
});


//Volume up - Volume Down Video

$(document).ready(function() {
  $("#volumeUp").hide();
  $("#volumeUp,#volumeOff").click(function() {
    if ($("video").prop('muted')) {
      $("video").prop('muted', false);
      $("#volumeUp").fadeIn(1000);
      $("#volumeOff").hide();
    } else {
      $("video").prop('muted', true);
      $("#volumeUp").hide();
      $("#volumeOff").fadeIn(1000);
    }
  });
});
