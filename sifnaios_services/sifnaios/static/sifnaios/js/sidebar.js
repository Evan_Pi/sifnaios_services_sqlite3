var close = $("#closeSidebar" ,"#sidebar");
var open = $("#openSidebar");
var sidebar = $("#sidebar");
var sidebarLinks = $("#sidebarLinks" ,"#sidebar");
var smNavbarBtn = $("#smNavbarBtn");
var secondarySidebar = $("#secondarySidebar");
var ulAClose = $("#ulAClose","#secondarySidebar");
var ulBClose = $("#ulBClose","#secondarySidebar");
var ulCClose = $("#ulCClose","#secondarySidebar");
var ulDClose = $("#ulDClose","#secondarySidebar");
var ulEClose = $("#ulEClose","#secondarySidebar");

//main SIDEBAR jQuery
$(document).ready(function(){

    close.hide();
    sidebarLinks.hide();

      open.click(function() {
        $("#sidebarWrapper").fadeIn(800);
        sidebar.animate({width: '90vw'});
        close.fadeIn(1800);
        sidebarLinks.fadeIn(1800);
      });

    smNavbarBtn.click(function() {
      $("#sidebarWrapper").fadeIn(800);
      sidebar.animate({width: '90vw'});
      close.fadeIn(1800);
      sidebarLinks.fadeIn(1800);
    });

    close.click(function() {
      $("#sidebarWrapper").hide();
      sidebar.animate({width: '0px'});
      close.hide();
      sidebarLinks.hide();
    });

    $("#sidebarWrapper").click(function() {
      $("#sidebarWrapper").hide();
      $("#ulA").hide();
      $("#ulC").hide();
      sidebar.animate({width: '0px'});
      secondarySidebar.animate({width: '0px'});

      close.hide();
      sidebarLinks.hide();
    });


//SECONDARY SIDEBAR

    $("#profile").click(function(){
      $("#ulA").fadeIn(1800);
      secondarySidebar.animate({width:'90vw'});
      ulAClose.fadeIn(1800);
      });
      ulAClose.click(function(){
        secondarySidebar.animate({width:'0%'});
        $(this).hide()
        $("#ulA").hide();
      });

    $("#technology").click(function(){
      $("#ulC").fadeIn(1800);
      secondarySidebar.animate({width:'90vw'});
      ulCClose.fadeIn(1800);
    });
    ulCClose.click(function(){
      secondarySidebar.animate({width:'0%'});
      $(this).hide()
      $("#ulC").hide();
    });

    $("#equipment").click(function(){
      $("#ulD").fadeIn(1800);
      secondarySidebar.animate({width:'90vw'});
      ulDClose.fadeIn(1800);
    });
    ulDClose.click(function(){
      secondarySidebar.animate({width:'0%'});
      $(this).hide()
      $("#ulD").hide();
    });

//NAVIGATION BUTTONS jQuery

    $(window).scroll(function() {
      if ($(window).scrollTop() < $(".wrapper").height() ) {
        $('#smNavbarBtn').hide();
      } else {
        $('#smNavbarBtn').show();
      }
    });

    $(window).scroll(function() {
      if ($(window).scrollTop() < 100) {
        $('#goTop').hide();
      } else {
        $('#goTop').show();
      }
    });
    $('#goTop').click(function() {
      $('html, body').animate({
        scrollTop: 0
      }, 800);
    });
    });
