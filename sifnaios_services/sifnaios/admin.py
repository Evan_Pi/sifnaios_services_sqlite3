from django.contrib import admin
from . models import equipment_categories , equipment , DTV_FORMERS , LG_model , CCTV , alarm_systems , Antenas
#Register your models here.

admin.site.register(equipment_categories)
admin.site.register(equipment)
admin.site.register(DTV_FORMERS)
admin.site.register(LG_model)
admin.site.register(CCTV)
admin.site.register(alarm_systems)
admin.site.register(Antenas)

admin.site.site_header= 'Sifnaios Administration'
