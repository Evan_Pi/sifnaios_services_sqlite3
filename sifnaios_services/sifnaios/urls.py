from django.urls import path
from . import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path( '' , views.index , name = 'index') ,
    path( 'Profile' , views.Profile , name = 'Profile') ,
    path( 'payInfo' , views.payInfo , name = 'payInfo') ,
    path( 'offers' , views.offers , name = 'offers') ,
    path( 'TermsOfUse' , views.TermsOfUse , name = 'TermsOfUse') ,
    path( 'Contact' , views.Contact , name = 'Contact') ,
    path( 'Services' , views.Services , name = 'Services') ,
    path( 'LGproducts' , views.LGproducts , name = 'LGproducts') ,
    path( 'TVSignal' , views.TVSignal , name = 'TVSignal') ,
    path( 'RackCabins' , views.RackCabins , name = 'RackCabins') ,
    path( 'cctv' , views.cctv , name = 'cctv') ,
    path( 'AlarmSystems' , views.AlarmSystems , name = 'AlarmSystems') ,
    path( 'Antenas' , views.Antenas , name = 'Antenas') ,
    path( 'VideowallTVBases' , views.VideowallTVBases , name = 'VideowallTVBases') ,
    path( 'Matrix' , views.Matrix , name = 'Matrix') ,
    path( 'Equipment' , views.Equipment , name = 'Equipment') ,
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
