# Generated by Django 2.1.2 on 2019-01-25 11:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sifnaios', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipment',
            name='name',
            field=models.CharField(blank=True, max_length=64),
        ),
    ]
