from django.db import models

class equipment_categories(models.Model):
    class Meta:
        verbose_name_plural = "Κατηγορίες εξοπλισμού"
    name = models.CharField(max_length=64)
    def __str__(self):
        return self.name



class equipment(models.Model):
    class Meta:
        verbose_name_plural = "Εξοπλισμός"
    category = models.ForeignKey(equipment_categories , on_delete = models.CASCADE)
    image = models.ImageField(upload_to = "equipment_images")
    name = models.CharField(max_length=64 , blank=True)
    description = models.TextField(max_length=2000 , blank=True)
    dimensions = models.CharField(max_length=64 , blank=True)
    prize = models.CharField(max_length=64 , blank=True)
    def __str__(self):
        return f"{self.name}"



class DTV_FORMERS(models.Model):
    class Meta:
        verbose_name_plural = "DTV διαμορφωτές"
    name = models.CharField(max_length=64)
    description = models.TextField(max_length=2000)
    image = models.ImageField(upload_to = "DTV_FORMERS")
    google_docs_link = models.CharField(max_length=1000 , default='' , blank=True)
    def __str__(self):
        return self.name



class LG_model(models.Model):
    class Meta:
        verbose_name_plural = "Προϊόντα LG"
    name = models.CharField(max_length=64)
    description = models.TextField(max_length=2000)
    image = models.ImageField(upload_to = "LG_models")
    google_docs_link = models.CharField(max_length=1000 , default='' , blank=True)
    def __str__(self):
        return self.name



class CCTV(models.Model):
    class Meta:
        verbose_name_plural = "CCTV"
    name = models.CharField(max_length=64)
    description = models.TextField(max_length=2000)
    image = models.ImageField(upload_to = "CCTV")
    google_docs_link = models.CharField(max_length=1000 , default='' , blank=True)
    def __str__(self):
        return self.name



class alarm_systems(models.Model):
    class Meta:
        verbose_name_plural = "Συστήματα Συναγερμού"
    category = models.CharField(max_length=64, blank=True)
    name = models.CharField(max_length=64, blank=True)
    description = models.TextField(max_length=2000, blank=True)
    image = models.ImageField(upload_to = "alarm_systems")
    def __str__(self):
        return f"{self.category} - {self.name}"


class Antenas(models.Model):
    class Meta:
        verbose_name_plural = "Κεραιοσυστήματα"
    category = models.CharField(max_length=64, blank=True)
    name = models.CharField(max_length=64, blank=True)
    description = models.TextField(max_length=2000, blank=True)
    image = models.ImageField(upload_to = "Antenas")
    def __str__(self):
        return f"{self.category}"
