from django.shortcuts import render
from . models import equipment , equipment_categories , DTV_FORMERS ,LG_model , CCTV , alarm_systems
from .models import Antenas as Ant #because of same name for Class and function error
# Create your views here.
def index(request):
    return render( request , 'sifnaios/index.html' )

#company ---------------------------------------------------
def Profile(request):
    return render( request , 'sifnaios/company/Profile.html')

def payInfo(request):
    return render( request , 'sifnaios/company/payInfo.html')

def offers(request):
    return render( request , 'sifnaios/company/offers.html' )

def TermsOfUse(request):
    return render( request , 'sifnaios/company/TermsOfUse.html' )
#company ---------------------------------------------------


def Contact(request):
    return render( request , 'sifnaios/Contact.html' )

def Services(request):
    return render( request , 'sifnaios/Services.html' )

#technology_products -----------------------------------------------------------------
def LGproducts(request):
    tvs = LG_model.objects.all()
    return render( request , 'sifnaios/technology_products/LGproducts.html' , {"tvs":tvs} )

def TVSignal(request):
    dtvformers = DTV_FORMERS.objects.all()
    return render( request , 'sifnaios/technology_products/TVSignal.html' , {"dtvformers":dtvformers} )

def RackCabins(request):
    return render( request , 'sifnaios/technology_products/RackCabins.html' )

def cctv(request):
    cctvs = CCTV.objects.all()
    return render( request , 'sifnaios/technology_products/cctv.html' , {"cctvs": cctvs} )

def AlarmSystems(request):
    alarms = alarm_systems.objects.all()
    return render( request , 'sifnaios/technology_products/AlarmSystems.html' , {"alarms" : alarms})

def Antenas(request):
    antenas = Ant.objects.all()
    return render( request , 'sifnaios/technology_products/Antenas.html' , {"antenas" : antenas})

def VideowallTVBases(request):
    return render( request , 'sifnaios/technology_products/VideowallTVBases.html' )

def Matrix(request):
    return render( request , 'sifnaios/technology_products/Matrix.html' )
#technology_products -----------------------------------------------------------------

#equipment -------------------------------------------------------------------------------------------
def Equipment(request):
    chairs = equipment.objects.filter(category__name='Chairs')
    barstools = equipment.objects.filter(category__name='Barstools')
    tables = equipment.objects.filter(category__name='Tables')
    return render( request , 'sifnaios/equipment.html' , {'chairs':chairs , 'tables':tables , 'barstools':barstools} )

#equipment -------------------------------------------------------------------------------------------
