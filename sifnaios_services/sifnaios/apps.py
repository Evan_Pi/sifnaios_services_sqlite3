from django.apps import AppConfig


class SifnaiosConfig(AppConfig):
    name = 'sifnaios'
